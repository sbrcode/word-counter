## wordcount
copy of unix wc command

## Summary
Print the number of lines, words and bytes for each file
and a row with totals if more than one file is specified.

## prerequisites:
python3

## Use examples :
```unix
$ python3 wc.py * zzz
53  134    1103 fib.py
32  112     871 prime.py
ro-file: Permission denied
40  139     855 stats.py
tmp: Is a directory
23  110     710 wc.py
zzz: No such file or directory
148 495    3539 total
```