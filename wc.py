import sys

# initialisation des variables qui permettront le calcul des totaux si plusieurs fichiers
total_ligne = total_mot = total_octet = 0

# boucle for qui va ouvrir et analyser chaque fichier
for nom_fichier in sys.argv[1:]:
    nb_ligne = 0
    liste_mot = liste_octet = []
    try:
        with open(nom_fichier, 'r') as fichier:
            for ligne in fichier:
                # on compte le nb de lignes en incrémentant à chauqe ligne du fichier
                nb_ligne += 1
                # le nb de mots sera décompté avec la liste des mots de chaque ligne
                liste_mot = liste_mot + ligne.split()
                # le nb d'octets sera représenté par une liste de tous les caractères du fichier mais encodé
                liste_octet += ligne.encode(encoding='utf8', errors='ignore')
            nb_mot = len(liste_mot)
            nb_octet = len(liste_octet)
            # affichage des informations pour chaque fichier
            print("\t{}\t{}\t{}\t{}".format(nb_ligne, nb_mot, nb_octet, nom_fichier))
        total_ligne += nb_ligne
        total_mot += nb_mot
        total_octet += nb_octet
    except IsADirectoryError:
        print("{}: {}: is a directory".format(sys.argv[0],nom_fichier))
        print("\t0\t0\t0\t{}".format(nom_fichier))
    except PermissionError:
        print ("{}: {}: Permission denied".format(sys.argv[0],nom_fichier))
    except FileNotFoundError:
        print("{}: {}: No such file or directory".format(sys.argv[0],nom_fichier))

# Affichage du total à l'identique de la commande unix
if len(sys.argv) > 2:
    print("\t{}\t{}\t{}\ttotal".format(total_ligne, total_mot, total_octet))